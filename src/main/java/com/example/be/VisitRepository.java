package com.example.be;

import org.springframework.data.repository.CrudRepository;

public interface VisitRepository extends CrudRepository<Visit, String> {
}