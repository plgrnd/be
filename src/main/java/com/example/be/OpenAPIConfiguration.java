package com.example.be;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class OpenAPIConfiguration implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**");
    }

    @Bean
    public OpenAPI configure() {
        return new OpenAPI()
                .info(new Info()
                        .title(OpenAPIConfiguration.class.getPackage().getImplementationTitle())
                        .version(OpenAPIConfiguration.class.getPackage().getImplementationVersion())
                        .description("")
                );
    }
}
