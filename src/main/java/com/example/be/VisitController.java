package com.example.be;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class VisitController {

    private final VisitRepository repository;

    VisitController(VisitRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public Iterable<Visit> index() {
        return repository.findAll();
    }

    @PostMapping("/")
    public Visit create(HttpServletRequest request) {
        return repository.save(new Visit(request.getRemoteAddr()));
    }

    @PatchMapping("/")
    public Visit update() {
        throw new RuntimeException("Not implemented");
    }
}
