FROM maven:3.8.6-openjdk-11 AS build
WORKDIR /workspace/app
ADD https://github.com/open-telemetry/opentelemetry-java-instrumentation/releases/latest/download/opentelemetry-javaagent.jar .
COPY pom.xml .
RUN mvn -B -e -C -T 1C org.apache.maven.plugins:maven-dependency-plugin:3.0.2:go-offline
COPY . .
RUN mvn clean package -Dmaven.test.skip=true

FROM eclipse-temurin:17.0.7_7-jre
VOLUME /tmp
ARG DEPENDENCY=/workspace/app/target/dependency
COPY --from=build /workspace/app/target/be.jar app.jar
COPY --from=build /workspace/app/opentelemetry-javaagent.jar opentelemetry-javaagent.jar
EXPOSE 9000
ENV JAVA_TOOL_OPTIONS "-javaagent:./opentelemetry-javaagent.jar"
ENTRYPOINT ["java", "-jar", "app.jar"]
