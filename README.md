# BE
Spring Boot dummy backend.

## Development
```shell
docker compose build
docker compose up
curl -X POST localhost:9000
curl localhost:9000
```

## Endpoints
- GET / 
- POST /
- PATCH /

## OpenAPI
- [Spec](localhost:9000/v3/api-docs) 
- [Swagger UI](localhost:9000/swagger-ui.html) 
